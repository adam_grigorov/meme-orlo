import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotificationService} from '../util/notification.service';

@Component({
    selector: 'app-image-drop',
    templateUrl: './image-upload.component.html',
    styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent implements OnInit {

    @Input()
    public length = 100;

    @Input()
    public preloadedImages: Array<ImageFile>;

    public dragover = false;

    public images: Array<ImageFile> = [];

    @Output()
    public imagesUploaded = new EventEmitter<Array<ImageFile>>();

    @Output()
    public removedImage = new EventEmitter<ImageFile>();

    @Input()
    public showPreload = true;

    constructor(private notifications: NotificationService) {
    }

    ngOnInit() {
        if (!!this.preloadedImages) {
            if (this.preloadedImages.length > this.length) {
                this.notifications
                    .message(`Can not pre upload images max length is: ${this.length} but pre upload ${this.preloadedImages.length}`);
            } else {
                this.preloadedImages.forEach(image => {
                    this.images.unshift(image);
                });
            }
        }

        if (this.length < 0) {
            this.length = 1;
        }
    }

    onDragOver(event: DragEvent): void {
        event.stopPropagation();
        event.preventDefault();
        this.dragover = true;
    }

    onDragleave(event: DragEvent): void {
        event.preventDefault();
        this.dragover = false;
    }


    onDrop(event: DragEvent): void {
        event.preventDefault();
        this.dragover = false;
        const files: FileList = event.dataTransfer.files;
        this.addFiles(files);
    }


    fileInputChange(files: FileList): void {
        this.addFiles(files);
    }


    addFiles(files: FileList) {

        if (this.images.length >= this.length) {
            this.notifications.message(`Max count of images is: ${this.length}`);
            return;
        }

        for (let i = 0; i < files.length; i++) {
            const reader = new FileReader();
            const file = files.item(i);

            const imageHolder: ImageFile = {
                name: file.name,
                size: file.size,
                type: file.type,
                src: ''
            };
            reader.onload = () => {
                imageHolder.src = reader.result;
                if (this.validateFile(imageHolder)) {
                    this.images.push(imageHolder);

                    if (i === files.length - 1) {
                        this.imagesUploaded.emit(this.images);
                    }
                }
            };
            reader.readAsDataURL(files.item(i));
        }
    }

    validateFile(file: ImageFile): boolean {
        let status = true;

        this.images.map(img => img.name).forEach(name => {
            if (file.name === name) {
                this.notifications.message('Some image already uploaded');
                status = false;
            }
        });

        return status;
    }

    removeImage(image): void {
        this.images.splice(this.images.indexOf(image), 1);
        this.removedImage.emit(image);
    }
}

export interface ImageFile {
    id?: number;
    name?: string;
    size?: number;
    type?: string;
    src: any;
}
