import {ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {EventService} from '../../util/events/event.service';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';
import {fabric} from 'fabric';
import {Canvas, Image} from 'fabric/fabric-impl';
import {NotificationService} from '../../util/notification.service';
import {FirebaseService} from '../../util/firebaseService/firebase.service';
import UploadTaskSnapshot = firebase.storage.UploadTaskSnapshot;

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

    public imageUrl = '';
    private canvas: Canvas = null;
    private backgroundImage: Image;
    public isDrawingMode = false;
    private resizeTimeOut: any = null;
    public imageToDownload;
    public textColor = 'white';
    public ready = false;
    private readyImageUrl: string;


    constructor(private eventService: EventService,
                private router: Router,
                private cd: ChangeDetectorRef,
                private notifications: NotificationService,
                private firebaseService: FirebaseService) {
    }

    ngOnInit() {
        this.eventService.onImageProvided.pipe(first()).subscribe(value => {
            if (!value) {
                this.router.navigate(['']);
            }
            this.imageUrl = value;
            this.initCanvas();
        });


    }

    initCanvas(): void {
        this.canvas = new fabric.Canvas('editorCanvas', {
            selection: true,
            backgroundColor: '#7b8087',
            selectionBorderColor: 'blue',
            containerClass: 'canvas-container',
            stopContextMenu: true,
            isDrawingMode: this.isDrawingMode,
        });

        fabric.Image.fromURL(this.imageUrl, img => {
            if (!img.width || img.width === 0) {
                this.router.navigate(['']);
                this.notifications.message('Cannot load this image :(');
            }
            this.backgroundImage = img;
            this.resizeCanvas();
            this.canvas.freeDrawingBrush.width = img.width / 48;
            this.canvas.freeDrawingBrush.color = 'red';
            this.setWatermarkText();

        }, {
            crossOrigin: 'anonymous'
        });


    }

    setWatermarkText() {
        const watermarkText = new fabric.IText('orlo.fun', {
            left: this.backgroundImage.width - (this.backgroundImage.width / 8),
            top: this.backgroundImage.height - 30,
            lockMovementX: true,
            lockMovementY: true,
            fill: 'white',
            selectable: false,
            fontSize: this.backgroundImage.width / 32,
            shadow: new fabric.Shadow({
                color: 'black',
                blur: 1,
                affectStroke: true
            })
        });
        this.canvas.add(watermarkText);
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (!!this.backgroundImage) {
            if (!!this.resizeTimeOut) {
                clearTimeout(this.resizeTimeOut);
            }
            this.resizeTimeOut = setTimeout(() => {
                this.resizeCanvas();
            }, 500);

        }
    }

    resizeCanvas() {
        const width = this.backgroundImage.width >= window.innerWidth ? window.innerWidth * 0.9 : this.backgroundImage.width;
        const height = this.backgroundImage.height;

        this.canvas.setWidth(width);
        this.canvas.setHeight(height * this.canvas.getWidth() / this.backgroundImage.width);

        this.canvas.setBackgroundImage(this.backgroundImage, this.canvas.renderAll.bind(this.canvas), {
            scaleX: 1,
            scaleY: 1,
        });
        this.canvas.setZoom(this.canvas.getWidth() / this.backgroundImage.width);
    }

    addText(): void {
        const iText = new fabric.Textbox('Your Text', {
            left: 50,
            top: 50,
            fontFamily: 'impact',
            fill: this.textColor,
            cornerSize: 15,
            transparentCorners: false,
            strokeWidth: 5,
            cornerColor: '#1244ea',
            textAlign: 'center',
            cornerStrokeColor: 'white',
            fontSize: this.backgroundImage.width / 8,
            shadow: new fabric.Shadow({
                color: this.textColor === 'black' ? 'white' : 'black',
                blur: 3,
                offsetX: 1,
                offsetY: 1,
            })
        });
        this.canvas.add(iText);
    }

    deleteSelected() {
        this.canvas.getActiveObjects().forEach(canvasObj => {
            this.canvas.remove(canvasObj);
        });
    }

    getImageDataURL(): string {
        if (!this.ready) {
            return '';
        }
        return this.canvas.toDataURL({
            format: 'png',
            left: 0,
            top: 0,
        });
    }

    download(downloadTrigger) {
        this.imageToDownload = this.getImageDataURL();
        this.cd.detectChanges();
        downloadTrigger.click();
        this.notifications.message('Subscribe on us in Telegram and Instagram');
    }

    shareToTelegram() {
        if (!!this.readyImageUrl) {
            this.openTelegram(this.readyImageUrl);
        } else {
            this.firebaseService.saveImage(this.getImageDataURL()).then((resp: UploadTaskSnapshot) => {
                resp.ref.getDownloadURL().then(url => {
                    this.readyImageUrl = encodeURIComponent(url);
                    this.openTelegram(this.readyImageUrl);
                });
            });
        }

        this.notifications.message('Subscribe on us in Telegram and Instagram');
    }

    copyToClickBoard() {
        if (!!this.readyImageUrl) {
            this.copyText(decodeURIComponent(this.readyImageUrl));
            this.notifications.message('Link copied');
        } else {
            this.firebaseService.saveImage(this.getImageDataURL()).then((resp: UploadTaskSnapshot) => {
                resp.ref.getDownloadURL().then(url => {
                    this.readyImageUrl = encodeURIComponent(url);
                    this.copyText(decodeURIComponent(this.readyImageUrl));
                    this.notifications.message('Link copied');
                });
            });
        }
    }

    copyText(val: string) {
        const selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }


    openTelegram(imageUrl: string): void {
        window.open(`https://telegram.me/share/url?url=${imageUrl}&text=orlo.fun`, '_blank');
    }

    colorChanged(): void {
        this.canvas.getActiveObjects().forEach(canvasObj => {
            if (canvasObj.get('type') === 'textbox') {
                canvasObj.setColor(this.textColor);
                canvasObj.shadow = new fabric.Shadow({
                    color: this.textColor === 'black' ? 'white' : 'black',
                    blur: 3,
                    offsetX: 1,
                    offsetY: 1,
                });
                this.canvas.renderAll();
            }
        });
    }

    changeDrawingState(state: boolean): void {
        this.canvas.isDrawingMode = state;
        this.isDrawingMode = state;
    }

}
