import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditorComponent} from './editor/editor.component';
import {MaterialModule} from '../matereal/material.module';
import {FlexModule} from '@angular/flex-layout';


@NgModule({
    declarations: [EditorComponent],
    imports: [
        CommonModule,
        MaterialModule,
        FlexModule
    ]
})
export class EditorModule {
}
