import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard/dashboard.component';
import {EditorComponent} from './editor/editor/editor.component';


const routes: Routes = [
    {path: '', pathMatch: 'full', component: DashboardComponent},
    {path: 'editor', pathMatch: 'full', component: EditorComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
