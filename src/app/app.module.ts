import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutModule} from './layout/layout.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {FirebaseService} from './util/firebaseService/firebase.service';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorage} from '@angular/fire/storage';
import {FlexModule} from '@angular/flex-layout/flex';
import {GridModule} from '@angular/flex-layout/grid';
import {EventService} from './util/events/event.service';
import {HttpClientModule} from '@angular/common/http';
import {NotificationComponent} from './util/notification/notification.component';
import {NotificationService} from './util/notification.service';

@NgModule({
    declarations: [
        AppComponent,
        NotificationComponent
    ],
    imports: [
        FlexModule,
        GridModule,
        BrowserModule,
        AppRoutingModule,
        LayoutModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        HttpClientModule
    ],
    entryComponents: [NotificationComponent],
    providers: [AngularFireStorage, FirebaseService, EventService, NotificationService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
