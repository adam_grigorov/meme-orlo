import {Component, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(private matIconRegistry: MatIconRegistry,
                private domSanitizer: DomSanitizer,
                private router: Router) {
        this.matIconRegistry.addSvgIcon(
            `telegram`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/images/telegram.svg')
        );
        this.matIconRegistry.addSvgIcon(
            `instagram`,
            this.domSanitizer.bypassSecurityTrustResourceUrl('../../../assets/images/instagram.svg')
        );
    }

    ngOnInit() {
    }

    goHome() {
        this.router.navigate(['']);
    }

    goToTelegram() {
        window.open(environment.links.telegram, '_blank');
    }

    goToInstagram() {
        window.open(environment.links.instagram, '_blank');
    }
}
