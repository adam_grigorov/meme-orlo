import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardModule} from '../dashboard/dashboard.module';
import {LayoutComponent} from './layout/layout.component';
import {RouterModule} from '@angular/router';
import {EditorModule} from '../editor/editor.module';
import {MaterialModule} from '../matereal/material.module';
import {FlexModule} from '@angular/flex-layout';


@NgModule({
    declarations: [LayoutComponent],
    imports: [
        DashboardModule,
        CommonModule,
        RouterModule,
        EditorModule,
        MaterialModule,
        FlexModule
    ],
    exports: [LayoutComponent]
})
export class LayoutModule {
}
