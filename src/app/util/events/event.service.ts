import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    private imageProvider: BehaviorSubject<string> = new BehaviorSubject<string>('');

    public onImageProvided: Observable<string> = this.imageProvider.asObservable();


    constructor() {
    }


    public provideNextImage(imageUrl): void {
        this.imageProvider.next(imageUrl);
    }
}
