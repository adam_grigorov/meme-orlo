import {Injectable} from '@angular/core';
import {AngularFireStorage} from '@angular/fire/storage';
import {HttpClient} from '@angular/common/http';
import 'firebase/storage';
import * as uuid from 'uuid';
import ListResult = firebase.storage.ListResult;
import UploadTask = firebase.storage.UploadTask;
import StringFormat = firebase.storage.StringFormat;
import * as firebase from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    constructor(private db: AngularFireStorage,
                private http: HttpClient) {
    }

    public getListOfImages(): Promise<ListResult> {
        return this.db.storage.ref('image-templates').listAll();
    }

    public saveImage(imageDataUrl: string): UploadTask {
        return this.db.storage.ref('ready').child(uuid.v4()).putString(imageDataUrl, StringFormat.DATA_URL, {contentType: 'image/png'});
    }


}


export interface ImageMeta {
    url: string;
    name: string;
}
