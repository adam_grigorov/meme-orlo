import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {NotificationComponent} from './notification/notification.component';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private config: MatSnackBarConfig = {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top'
    };

    constructor(private snackBar: MatSnackBar) {
    }

    message(message: string) {
        let config = this.config;
        config.data = {message: message};
        this.snackBar.openFromComponent(NotificationComponent, config);
    }


}
