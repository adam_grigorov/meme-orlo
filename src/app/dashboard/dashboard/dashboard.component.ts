import {Component, OnInit} from '@angular/core';
import {FirebaseService, ImageMeta} from '../../util/firebaseService/firebase.service';
import {Router} from '@angular/router';
import {EventService} from '../../util/events/event.service';
import ListResult = firebase.storage.ListResult;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    public images: Array<ImageMeta> = [];

    constructor(private fireService: FirebaseService,
                private router: Router,
                private eventService: EventService) {
    }

    ngOnInit() {
        this.fireService.getListOfImages().then((result: ListResult) => {
            result.items.forEach(item => {
                item.getDownloadURL().then(imageUrl => {
                    this.images.push({name: item.name, url: imageUrl});
                });
            });
        });
    }

    goToEdit(imageUrl): void {
        this.router.navigate(['editor']).then(r => this.eventService.provideNextImage(imageUrl));
    }

}
