import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MaterialModule} from '../matereal/material.module';
import {FlexModule, GridModule} from '@angular/flex-layout';


@NgModule({
    declarations: [DashboardComponent],
    imports: [
        GridModule,
        MaterialModule,
        CommonModule,
        FlexModule
    ],
    exports: [
        DashboardComponent
    ]
})
export class DashboardModule {
}
