export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyBVZiXWOoVYNiQCw9fOMMi6EbxfMz2Batk',
        authDomain: 'orlo-d57cc.firebaseapp.com',
        databaseURL: 'https://orlo-d57cc.firebaseio.com',
        projectId: 'orlo-d57cc',
        storageBucket: 'orlo-d57cc.appspot.com',
        messagingSenderId: '399324852762',
        appId: '1:399324852762:web:fc32c043df542f2b4c17a3',
        measurementId: 'G-5BHY1J06DR'
    },
    links: {
        telegram: 'https://t.me/orlo_blet',
        instagram: 'https://www.instagram.com/_adam_pv'
    }
};
